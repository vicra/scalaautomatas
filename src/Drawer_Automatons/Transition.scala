package Drawer_Automatons


import scalafx.scene.canvas.Canvas
import scalafx.scene.paint.Color

class Transition() {

  var canvas:Canvas = new Canvas()
  var originX:Int = 0
  var originY:Int = 0
  var destinyX:Int = 0
  var destinyY:Int = 0
  var origin:Node = new Node
  var destiny:Node = new Node
  var value:String = ""

  def this( origin_x:Int,origin_y:Int,destiny_x:Int,destiny_y:Int,canvas:Canvas){
    this()
    this.canvas = canvas
    this.originX = origin_x
    this.originY  = origin_y
    this.destinyX = destiny_x
    this.destinyY = destiny_y
  }
  def this(origin:Node, destiny:Node,value:String,canvas:Canvas){
    this()
    this.canvas = canvas
    this.originX = origin.centerX
    this.originY  = origin.centerY
    this.destinyX = destiny.centerX
    this.destinyY = destiny.centerY
    this.origin = origin
    this.destiny = destiny
    this.value = value
  }
  def draw_transition(): Unit ={
    val gc = canvas.graphicsContext2D
    gc.fill = Color.Blue
    if(origin.name == destiny.name) {
      gc.strokeOval(originX.toDouble-30, originY.toDouble-70, 40,70)
      gc.strokeText(value,originX-20,originY-85)
      gc.stroke = Color.Red
      gc.strokeOval(originX-30,originY-10,10,10)
      gc.stroke = Color.Black
    }
    else{
      val middleX: Int = (originX + destinyX) / 2
      val middleY: Int = (originY + destinyY) / 2
      gc.strokeLine(originX,originY,destinyX,destinyY)
      gc.strokeText(value,middleX,middleY)

      val arrowX: Int = (middleX + destinyX) / 2
      val arrowY: Int = (middleY + destinyY) / 2
      gc.stroke = Color.Red
      gc.strokeOval(arrowX, arrowY,10,10)
      gc.stroke = Color.Black

    }
  }
  def collision(x:Double, y:Double,node:String): Boolean ={
    val offset: Int = 25
    if(node == "origin"){
      if(x >= this.originX - offset && x <= this.originX + offset){
        if(y >= this.originY - offset && y <= this.originY + offset){
          return true
        }
      }
    }else if(node == "destiny"){
      if(x >= this.destinyX - offset && x <= this.destinyX + offset){
        if(y >= this.destinyY - offset && y <= this.destinyY + offset){
          return true
        }
      }
    }
    return false
  }
  def move(x:Double, y:Double, node:String): Unit ={
    if(node == "origin"){
      this.originX = x.toInt
      this.originY = y.toInt
    }else if(node == "destiny"){
      this.destinyX= x.toInt
      this.destinyY = y.toInt
    }
  }
}
