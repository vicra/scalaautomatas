package Drawer_Automatons

import scalafx.scene.canvas.Canvas
import scalafx.scene.paint.Color

class Node(){

  var canvas:Canvas = new Canvas()
  var acceptance:Boolean = false
  var initial:Boolean = false
  var name:String = ""

  var x:Int = 10
  var y:Int = 10
  var width:Int = 60
  var height:Int = 60
  var radius:Int = width/2
  var centerX:Int = x + radius
  var centerY:Int = y + radius

  var border_size:Int = 10
  var border_offset:Int = 5
  var x_border:Int = x - border_offset
  var y_border:Int = y - border_offset
  var border_width:Int = this.width + border_size
  var border_height:Int = this.height + border_size


  def this(canvas_state:Canvas, acceptance_state:Boolean, initial_state:Boolean,state_name:String){
    this()
    canvas = canvas_state
    acceptance = acceptance_state
    initial = initial_state
    name = state_name
  }

  def change_name(new_name:String): Unit ={
    this.name = new_name
  }

  def draw_circle(){
    val gc = canvas.graphicsContext2D

    gc.strokeOval(this.x,this.y,width,height)
    gc.fill = Color.Yellow

    if(acceptance){
      gc.strokeOval(x_border,y_border,border_width,border_height)
    }
    if(initial){
      gc.fill = Color.Green
    }
    gc.fillOval(this.x,this.y,width,height)
    gc.strokeText(name,this.centerX,this.centerY)
  }

  def move(x:Double, y:Double): Unit ={
    this.x = x.toInt
    this.y = y.toInt
    this.centerX = this.x + this.radius
    this.centerY = this.y + this.radius
    this.x_border = this.x - border_offset
    this.y_border = this.y - border_offset
  }
  def collision(x:Double, y:Double): Boolean ={
    if(x >= this.x && x <= this.x + width){
      if(y >= this.y && y <= this.y + height){
        return true
      }
    }
    return false
  }
}