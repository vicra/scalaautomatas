package Drawer_Automatons

import Automatons.DFA

import scala.collection.mutable.ListBuffer
import scalafx.Includes._
import scalafx.application.JFXApp
import scalafx.event.ActionEvent
import scalafx.scene.Scene
import scalafx.scene.canvas.Canvas
import scalafx.scene.control.Alert.AlertType
import scalafx.scene.control._
import scalafx.scene.paint.Color
import scalafx.scene.input.{DragEvent, MouseDragEvent, MouseEvent}

object MainApp extends JFXApp {
  val dfa = new DFA()
  dfa.set_alphabet(ListBuffer('0','1'))
  var counter:Int = 0

  var nodes:ListBuffer[Node] = ListBuffer[Node]()
  var transitions:ListBuffer[Transition] = ListBuffer[Transition]()

  stage = new JFXApp.PrimaryStage {
    title.value = "DFA Automata"
    width = 1200
    height = 800
    scene = new Scene {
      fill = Color.Aquamarine

      val canvas = new Canvas(1000,600)
      canvas.layoutX = 350
      canvas.layoutY = 0
      val gc = canvas.graphicsContext2D

      def draw_canvas(): Unit ={
        for(trans <- transitions){
          trans.draw_transition()
        }
        for(node<-nodes){
          node.draw_circle()
        }
      }
      def clean_canvas(): Unit ={
        gc.clearRect(0,0,1000,1000)
      }

      canvas.onMouseDragged() = (e: MouseEvent) => {

        var x:Double = e.x
        var y:Double = e.y

        for(trans <- transitions){
          if(trans.collision(x,y,"origin")){
            trans.move(x,y,"origin")
          }
          else if(trans.collision(x,y,"destiny")){
            trans.move(x,y,"destiny")
          }
          clean_canvas()
        }
        for(node<- nodes){
          if(node.collision(x,y)){
            node.move(x-25,y-25)
            clean_canvas()
          }
          draw_canvas()
        }
      }
      canvas.onMouseClicked = (e: MouseEvent) => {
        draw_canvas()
      }


      val label = new Label("Create new State")
      label.layoutY = 50
      label.layoutX = 10

      val acceptance = new CheckBox("Acceptance State")
      acceptance.layoutY = 90
      acceptance.layoutX = 10

      val initial = new CheckBox("Initial State")
      initial.layoutY = 130
      initial.layoutX = 10

      val btn_new_state = new Button("Create State")
      btn_new_state.layoutY = 160
      btn_new_state.layoutX = 10


      val btn_draw_nodes = new Button("Draw Nodes")
      btn_draw_nodes.layoutY = 50
      btn_draw_nodes.layoutX = 200

      val btn_draw_functions = new Button("Draw functions")
      btn_draw_functions.layoutY = 100
      btn_draw_functions.layoutX = 200

      val label2 = new Label("Create new Function")
      label2.layoutY = 250
      label2.layoutX = 10

      val originState_comboBox = new ComboBox[String]()
      originState_comboBox.layoutY = 280
      originState_comboBox.layoutX = 100

      val destinyState_comboBox = new ComboBox[String]()
      destinyState_comboBox.layoutY = 320
      destinyState_comboBox.layoutX = 100

      val alphabetValue_ComboBox = new ComboBox[Char]()
      alphabetValue_ComboBox.layoutY = 360
      alphabetValue_ComboBox.layoutX = 100

      val label3 = new Label("Origin")
      label3.layoutY = 280
      label3.layoutX = 10

      val label4 = new Label("Destiny")
      label4.layoutY = 320
      label4.layoutX = 10

      val label5 = new Label("Value")
      label5.layoutY = 360
      label5.layoutX = 10


      for(alphabet_character <- dfa.alphabet){
        alphabetValue_ComboBox += alphabet_character
      }

      val btn_add_function = new Button("Add Function")
      btn_add_function.layoutY = 400
      btn_add_function.layoutX = 10

      val btn_clear_automaton = new Button("Clear Automaton")
      btn_clear_automaton.layoutY = 700
      btn_clear_automaton.layoutX = 10



      content = List(
        label, acceptance, initial, btn_new_state,
        canvas,
        btn_draw_nodes,btn_clear_automaton,btn_draw_functions,
        label2,originState_comboBox,destinyState_comboBox,alphabetValue_ComboBox,btn_add_function,
        label3,label4,label5
      )
      btn_new_state.onAction = (e:ActionEvent) => {
        val state_name = asign_name()
        val node = new Node(canvas,acceptance.selected.value,initial.selected.value,state_name)
        acceptance.selected = false
        initial.selected = false
        nodes += node
        node.draw_circle()

        destinyState_comboBox += state_name
        originState_comboBox += state_name

        dfa.add_state(state_name,initial.selected.value,acceptance.selected.value)
        dfa.print_states()
      }
      btn_add_function.onAction = (e:ActionEvent) =>{
        var origin:String = originState_comboBox.getValue
        var destiny:String = destinyState_comboBox.getValue
        var value:Char = alphabetValue_ComboBox.getValue

        var transition = new Transition(get_node(origin),get_node(destiny),value.toString,canvas)
        transition.draw_transition()
        transitions+=transition
        draw_canvas()

        if(origin.nonEmpty && destiny.nonEmpty){
          if (dfa.add_function(origin, destiny, value)){
            new Alert(AlertType.Information, "Function added successfully").showAndWait()
          }
        }
        else{
          new Alert(AlertType.Information, "Missing Data").showAndWait()
        }
      }
      btn_draw_nodes.onAction = (e:ActionEvent) => {
        clean_canvas()
        draw_canvas()
      }
      btn_draw_functions.onAction = (e:ActionEvent) =>{
        new Alert(AlertType.Information, "Functions Drawed").showAndWait()
      }
        btn_clear_automaton.onAction = (e:ActionEvent) =>{
        nodes = ListBuffer()
        new Alert(AlertType.Information, "States and functions removed").showAndWait()
      }
    }
  }
  def asign_name():String = {

    val name_to_return = "q" + counter
    val new_counter = counter + 1
    counter = new_counter
    return name_to_return
  }
  def get_node(name:String):Node={
    for(node <- nodes){
      if(node.name == name){
        return node
      }
    }
    return null
  }

}

