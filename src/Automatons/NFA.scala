/**
  * Created by dell on 19/02/2016.
  */

package Automatons

import scala.collection.mutable.ListBuffer

class NFA extends Automaton{



  def add_function(origin_state_name:String,
                   destiny_state_name:String,
                   value:Char): Boolean =
  {
    val origin_state: State = get_state(origin_state_name)
    val destiny_state: State = get_state(destiny_state_name)
    if(origin_state!=null && destiny_state!=null){
      origin_state.add_function(value, destiny_state)
      return true
    }
    return false
  }
  def evaluate(string:String): Boolean ={

    var current_states: ListBuffer[State] = ListBuffer()
    current_states += this.get_initial_state()

    var next_states: ListBuffer[State] = ListBuffer()
    val char_array = string.toCharArray

    if(string.nonEmpty){
      for(character <- char_array){
        for (state <- current_states){
          next_states = state.get_destiny_states(character)
        }
        current_states = next_states
      }
      for(final_state <- current_states){
        if(final_state.acceptedState){
          return true
        }
      }
    }
    return false
  }


  def to_DFA(): DFA  ={
    val automata: DFA = new DFA

    var current_state:State = this.get_initial_state()
    for(symbol <- automata.alphabet){

      for(state_iterator <- automata.states){
        current_state = this.get_state(state_iterator.name)
        val destiny_states: ListBuffer[State] = current_state.get_destiny_states(symbol)

        var new_state:State = new State()
        var new_name:String = ""
        if(destiny_states!=null){
          for(destiny_states_iterator <- destiny_states){
            if(new_name.isEmpty){
              new_name += destiny_states_iterator.name
            }else{
              new_name += "," + destiny_states_iterator.name
            }
            if(destiny_states_iterator.acceptedState){
              new_state.setAcceptedState(true)
            }
          }
          new_state.set_name(new_name)
          automata.states += current_state
          automata.states += new_state
        }
        automata.add_function(current_state.name, new_state.name, symbol)
      }
    }
    return automata
  }
}
