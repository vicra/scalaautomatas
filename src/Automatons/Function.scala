
package Automatons

class Function(
                val value:Char,
                val final_state:State
              ) {
  val character:Char = value
  val finalState:State = final_state
}
