package Automatons

import scala.collection.mutable.ListBuffer

class State() {
  var name: String = ""
  var initialState:Boolean = false
  var acceptedState:Boolean = false
  var functionList: ListBuffer[Function] = ListBuffer()

  def this( state_name:String ,
   initial_state:Boolean,
   accepted_state:Boolean){
    this()
    var name: String = state_name
    var initialState:Boolean = initial_state
    var acceptedState:Boolean = accepted_state
    var functionList: ListBuffer[Function] = ListBuffer()
  }
  def setInitialState(bool:Boolean){
    initialState = bool
  }
  def setAcceptedState(bool:Boolean){
    acceptedState = bool
  }
  def add_function(value:Char,final_state:State){
    val new_function = new Function(value, final_state)
    functionList += new_function
  }
  def remove_function(value:Char,final_state:State){
    val function = get_function(value, final_state)
    functionList -= function
  }
  def set_name(new_name:String): Unit ={
    name = new_name
  }
  def exists_destination(value_to_check:Char): Boolean ={
    for(function_iterator <- functionList){
      if(function_iterator.value == value_to_check){
        return true
      }
    }
    return false
  }
  def get_destiny_state(value:Char): State ={
    for(function_iterator <- functionList){
      if(function_iterator.value == value){
        return function_iterator.finalState
      }
    }
    return null
  }
  def get_destiny_states(value:Char): ListBuffer[State] ={
    var destiny_states: ListBuffer[State] = ListBuffer()
    for(function_iterator <- functionList){
      if(function_iterator.value == value){
        //return function_iterator.finalState
        destiny_states += function_iterator.finalState
      }
    }
    return destiny_states
  }

  def clean_function_list(): Unit ={
    functionList = ListBuffer()
  }
  def get_function(value:Char,final_state:State):Function = {
    for(function_iterator <- functionList){//set the other states to not be initial state
      if(function_iterator.character == value &&
          function_iterator.finalState == final_state){
        return function_iterator
      }
    }
    return null
  }
  def print_functions(): Unit ={
    println("state Name:" + name)
    for(function_iterator <- functionList){
      println("Destiny :" + function_iterator.finalState.name)
      println("value: " + function_iterator.character)
    }
  }
}
