package Automatons

import scala.collection.mutable.ListBuffer

class Automaton(){
  var name: String = ""
  var states: ListBuffer[State] = ListBuffer()
  var alphabet: ListBuffer[Char] = ListBuffer()

  def this(automaton_name:String,alpha_bet: ListBuffer[Char] ){
    this()
    set_name(automaton_name)
    set_alphabet(alpha_bet)
  }
  def this(automaton_name:String){
    this()
    set_name(automaton_name)
  }
  def set_name(automaton_name:String): Unit ={
    this.name = automaton_name
  }
  def set_alphabet(alpha_bet:ListBuffer[Char]): Unit ={
    this.alphabet = alpha_bet
  }
  def set_acceptance_state(state_name:String,bool:Boolean): Unit ={
    val state = get_state(state_name)
    if(state != null){
      state.setAcceptedState(bool)
    }
  }
  def add_state(state_name:String, initial:Boolean, acceptance:Boolean){
    val new_state = new State(state_name, initial,acceptance )
    if(states.isEmpty){
      new_state.setInitialState(true)
    }
    states += new_state
  }
  def add_state(state_name:String): Unit ={
    val new_state = new State(state_name, false,false)
    if(states.isEmpty){
      new_state.setInitialState(true)
    }
    states += new_state
  }
  def remove_state(state_name:String) {
    val state = get_state(state_name)
    states -= state
  }
  def set_initial_state(state_name:String){
    val state = get_state(state_name)
    for(state_iterator <- states){//set the other states to not be initial state
      state_iterator.setInitialState(false)
    }
    state.setInitialState(true)
  }
  def get_state(state_name:String): State ={
    for(state_iterator <- states){//set the other states to not be initial state
      if(state_iterator.name == state_name){
        return state_iterator
      }
    }
    return null
  }
  def get_initial_state():State = {
    for(state_iterator <- states){//set the other states to not be initial state
      if(state_iterator.initialState == true){
        return state_iterator
      }
    }
    return null
  }
  def print_name(): Unit ={
    println(this.name)
  }
  def print_states(): Unit ={
    for(state <- states){
      println(state.name)
    }
  }
  def print_alphabet(): Unit ={
    for(character <- alphabet){
      println(character)
    }
  }
}
