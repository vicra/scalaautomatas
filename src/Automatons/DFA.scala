/**
  * Created by dell on 19/02/2016.
  */
package Automatons

class DFA extends Automaton {


  //returns true if function added
  def add_function(origin_state_name:String,
                   destiny_state_name:String,
                   value:Char): Boolean =
  {

    val origin_state: State = get_state(origin_state_name)
    val destiny_state: State = get_state(destiny_state_name)

    if(origin_state!= null  &&
      destiny_state != null &&
      !origin_state.exists_destination(value))
    {
      origin_state.add_function(value, destiny_state)
      return true
    }
    return false
  }

  def evaluate(string:String): Boolean ={
    val char_array = string.toCharArray
    val initial_state = get_initial_state()
    println("estado inicial: "+ initial_state.name)//******remover print
    var current_state:State = initial_state

    if(char_array.nonEmpty){ //si cadeno no es vacia
      for(character <- char_array){
        println(character)
        val new_state = current_state.get_destiny_state(character)
        if(new_state!= null){
          println(new_state.name)
          current_state = new_state
        }else{
          println("estado nullo")
        }
      }
      if(current_state.acceptedState){
        return true
      }
    }
    return false
  }

  def minimize(): DFA ={
    var minimized_DFA = new DFA
    minimized_DFA.states = this.states

    var current_state:State= this.get_initial_state()

    for(origin_state <- this.states){
      for(destiny_state <- this.states){
        if(destiny_state.name != origin_state.name){
          var equivalent = false
          for(symbol <- this.alphabet){
              if(origin_state.get_destiny_state(symbol) == destiny_state.get_destiny_state(symbol)){
                equivalent = true
              }else{
                equivalent = false
              }
          }
          if(equivalent){
            var new_state = new State
            new_state.name = origin_state.name + ", " + destiny_state.name
            minimized_DFA.states += new_state
            minimized_DFA.remove_state(origin_state.name)
            minimized_DFA.remove_state(destiny_state.name)

            //add same functions
          }
        }
      }
    }
    return minimized_DFA
  }
}
